# Grafana

## General
Grafana is a widely used data visualisation and charting tool which connects to Crate DB. Grafana is open source visualization and analytics software. It allows you to query, visualize, alert on, and explore your metrics no matter where they are stored. In plain English, it provides you with tools to turn your time-series database (TSDB) data into beautiful graphs and visualizations.

Original documentation can be found here: https://grafana.com/docs/grafana/v6.7/ 

## Usage of Grafana

Currently Grafana is not utilized, but provide an example of how to use within the [Testing Documentation](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/master/platform-testing).

Information on first steps in Grafana: https://grafana.com/docs/grafana/v6.7/getting-started/getting-started/

Information on how to create a data source and dashboard (regarding to CrateDB): https://crate.io/a/pair-cratedb-with-grafana-6-x-an-open-platform-for-time-series-data-visualization/

## Versioning  
Tested on:  
Version 6.7.2 (Commit: 423a25fc32) https://github.com/grafana/grafana/releases/tag/v6.7.2  

## Volume Usage
Grafana uses persistent directory for saving settings and dashboard configuration (`/var/lib/grafana`)  
PVC: NAMESPACE-grafanadata-STATEFULSET-NAME 

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
Grafana UI can be accessed via: https://grafana.fiware.opendata-CITY.de

## Access the UI
The Administration User is created during deployment (see k8s secret below).
This Administration User can create other users including other administrators.

## Further Testing
The image (https://hub.docker.com/layers/grafana/grafana/6.7.2/images/sha256-bdef6f27255a09deb2f89741b3800a9a394a7e9eefa032570760e5688dd00a2f?context=explore) is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for `dev`- and `staging`-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for `prod`-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s

INSTALLPLUGINS_DEV - Plugins to be installed into grafana in dev-namespace, separated by ','  
INSTALLPLUGINS_STAGING - Plugins to be installed into grafana in staging-namespace, separated by ','  
INSTALLPLUGINS_PROD - Plugins to be installed into grafana in prod-namespace, separated by ','  

SMTPHOST_DEV - SMTP-host for SMTP-functionality in dev-namespace, in format smtp.example.com:port.  
SMTPUSER_DEV - SMTP-user for previously mentioned host, in format user@example.com.  
SMTPPASS_DEV - SMTP-password for previously mentioned user.  


SMTPHOST_STAGING - SMTP-host for SMTP-functionality in staging-namespace, in format smtp.example.com:port.  
SMTPUSER_STAGING - SMTP-user for previously mentioned host, in format user@example.com  
SMTPPASS_STAGING - SMTP-password for previously mentioned user.  


SMTPHOST_PROD - SMTP-host for SMTP-functionality in production-namespace, in format smtp.example.com:port.  
SMTPUSER_PROD - SMTP-user for previously mentioned host, in format user@example.com.  
SMTPPASS_PROD - SMTP-password for previously mentioned user.  

	
GRAFANAURL_DEV - Grafana public address in dev-stage. In format https://grafana.example.com  
GRAFANAURL_STAGING - Grafana public address in staging-stage. In format https://grafana.example.com  
GRAFANAURL_PROD - Grafana public address in prod-stage. In format https://grafana.example.com  

### Kubernetes Secrets
Create kubernetes-secret for admin password
`kubectl create secret generic grafana-password --from-literal=password=YOURPASSWORD --namespace=fiware-dev|fiware-staging|fiware-prod`

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.

